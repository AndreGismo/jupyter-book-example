# Welcome to your Jupyter Book

This is a small sample book to give you a feel for how book content is
structured.

:::{note}
Here is my personal note!
:::

And here is my personal code block:

```
print('hello world')
```

Check out the content pages bundled with this sample book to see more.
